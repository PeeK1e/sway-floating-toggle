package util

import (
	"encoding/json"
	"fmt"
)

func InterfaceToJSON(i interface{}) (string, error) {
	// Check and cast the interface{} to map[string]interface{}
	data, ok := i.(map[string]interface{})
	if !ok {
		return "", fmt.Errorf("could not cast interface{} to map[string]interface{}")
	}

	// Now marshal the data into a JSON string
	jsonString, err := json.Marshal(data)
	if err != nil {
		return "", err
	}

	return string(jsonString), nil
}
