## Sway Floating Window Toggler

This is a tool for toggling the floating behavior of windows in the Sway tiling window manager. It does this by making use of the Sway IPC (Inter-Process Communication) interface to communicate with the Sway window manager and modify window properties.

### Dependencies

This program relies on the gosway/ipc package for connecting to Sway's IPC interface and the charmbracelet/log package for logging.

You can get these dependencies by running:

```sh
go get github.com/Difrex/gosway/ipc
go get github.com/charmbracelet/log
```

### Usage
To install:
```sh
go install gitlab.com/PeeK1e/sway-floating-toggle/cmd/toggle@main
```

And add this to your sway config:
```conf
#.sway/config
## replace $mod+M by your hotkey

bindsym $mod+M exec "$HOME/go/bin/toggle"
```