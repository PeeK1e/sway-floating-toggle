package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/PeeK1e/gosway/ipc"
	"github.com/charmbracelet/log"
	"gitlab.com/PeeK1e/sway-floating-toggle/pkg/util"
)

func main() {
	log.SetLevel(log.DebugLevel)
	log.SetReportCaller(true)
	log.SetOutput(os.Stdout)

	log.Info("Startup")
	i, err := ipc.NewSwayConnection()
	if err != nil {
		log.Fatal("Could not get ipc", err)
	}

	ws, err := i.GetFocusedWorkspace()
	if err != nil {
		log.Fatal("Could not get workspace", err)
	}
	floatingNodes := ws.FloatingNodes

	nodes, err := i.GetFocusedWorkspaceWindows()
	if err != nil {
		log.Fatal("Could not get windows", err)
	}

	for _, v := range floatingNodes {
		var node ipc.Node

		jsonString, err := util.InterfaceToJSON(v)
		if err != nil {
			log.Fatal(err)
		}

		if err := json.Unmarshal([]byte(jsonString), &node); err != nil {
			log.Fatal(err)
		}

		nodes = append(nodes, node)
	}

	var focused *ipc.Node
	for i, v := range nodes {
		log.Debug("Node", "number", i, "node", v)
		if v.Focused {
			focused = &v
			break
		}
	}

	if focused == nil {
		log.Info("No Windows found")
		return
	}
	log.Info("Got window", "id", focused.ID)

	floating := "enable"
	if focused.Type == "floating_con" {
		floating = "disable"
	}
	log.Info("Setting window", "class", focused.WindowProperties.Class, "name", focused.Name, "floating", floating)

	//command := fmt.Sprintf("[ con_id=%d ] floating %s", focused.ID, floating)
	command := ""
	if focused.WindowProperties.Class == "" {
		command = fmt.Sprintf("[ name=%s ] floating %s", focused.Name, floating)
	} else {
		command = fmt.Sprintf("[ class=%s ] floating %s", focused.WindowProperties.Class, floating)
	}
	out, err := i.RunSwayCommand(command)
	if err != nil {
		log.Fatal("Failed to run command", "command", command, "out", out, "err", err)
	}
}
